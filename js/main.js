$(document).ready(function(){
  "use strict";
  
    // Nav
    $('a.top__link').on('click', function(event) {
      $('.navbar-toggle:visible').trigger('click');
    });

    //only desktop
    var ww = $(window).width();
    if ( ww > 1024) { 
      $('body').mousemove(function(e) {
        var change,
        xpos = e.clientX,
        ypos = e.clientY,
        left =  change*20,
        xpos = xpos*2,
        ypos = ypos*2;
        // $('.intro__decor').css('transform',translate(((0+(ypos/50))+"px")));
        $('.intro__decor').css('top',((-35-(ypos/110))+"px")); 
        $('.intro__decor').css('right',(( -38+(xpos/120))+"px"));
      });
    };

    $('.menu-trigger').on('focusout', function(){
      $('.navbar-collapse').collapse('hide');
    });

    //anhors
    $(".nav__link").on('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
      }, 700, 'swing')
      event.preventDefault();
    });

    //slider
    $('.intro__slider').slick({
      responsive:true,
      infinite: true,
      dots: true,
      speed:500,
      arrows: true
    });

    //slider
    $('.blog__slider').slick({
      responsive:true,
      infinite: true,
      dots: true,
      speed:500,
      // fade:true,
      arrows: false
    });

     
});
